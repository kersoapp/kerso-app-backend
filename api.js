const express = require('express');
const bodyParser = require('body-parser');
/*  */
/* Ac� van los requires de los archivos requeridos */
/* const nombreArchivo = requires('ruta/a/nombrearchivo.js'); */

const app = express();

/* para poder recibir y usar el body y json */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.post('/api/nombreAccionPost', (req, res) => {
    /* acciones a ejecutar */
    /* let objetoQueLlega = req.body; */
})

app.get('/api/nombreAccionGet', (req, res) => {
    
})


//acciones sobre un usuario
app.post('/api/users/newUser', (req, res) => {
    let userObject = req.body;
    //post a la BD
    res(confrmacionBD);
})
  
  
app.post('/api/users/updateUser', (req, res) => {
    let userObject = req.body;
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/users/followUser', (req, res) => {
    let userObject = req.body;
    // El userObject traer�a el ID del usuario al que se va a seguir y el ID del usuario que sigue
    //userObject: {idUserDaLike, idUserRecibeLike}
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/users/login', (req, res) => {
    let userObject = req.body;
    // El userObject traer�a el ID del usuario que va a hacer login
    //userObject: {idUsuarioLogin}
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/users/logout', (req, res) => {
    let userObject = req.body;
    // El userObject traer�a el ID del usuario que va a hacer logout
    //userObject: {idUsuarioLogout}
    //post a la BD
    res(confrmacionBD);
})


//acciones sobre multimedia
app.post('/api/media/newVideo', (req, res) => {
    let videoObject = req.body;
    //post a la BD
    // El videoObject traer�a el video que se va a subir, el ID del usuario que lo sube y la descripci�n del video
    // videoObject: {videoBase64Encoded, idUsuario, descripci�n }
    res(confrmacionBD);
})


app.post('/api/media/deleteVideo', (req, res) => {
    let videoObject = req.body;
    // post a la BD
    // El videoObject traer�a el ID del video que se va a borrar y el ID del usuario que borra el video
    // videoObject: {idVideoEliminada, idUsuario}
    res(confrmacionBD);
})


app.post('/api/media/likeVideo', (req, res) => {
    let videoObject = req.body;
    // El videoObject traer�a el ID del usuario que public� el video, el ID del video al que se le dio like y
    // el ID del usuario que dio like al video
    // videoObject: {idUserDaLike, idVideoRecibeLike}
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/media/highlightVideo', (req, res) => {
    let videoObject = req.body;
    // El videoObject traer�a el ID del video que quiere remarcar
    // Los videos que se remarcan son los que aparecen al inicio del perfil de la persona en la secci�n de videos
    // (sin necesidad de mover el slider)
    // videoObject: {idVideo}
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/media/newImage', (req, res) => {
    let imageObject = req.body;
    // El imageObject traer�a la imagen que se va a subir, el ID del usuario que la sube y la descripci�n de la imagen
    // imagenObject: {imagenBase64Encoded, idUsuario, descripci�n }
    //post a la BD
    res(confrmacionBD);
})
  
    
app.post('/api/media/deleteImage', (req, res) => {
    let imageObject = req.body;
    //post a la BD
    // El imageObject traer�a el ID de la imagen que se va a borrar y el ID del usuario que borra la imagen
    // imageObject: {idImagenEliminada, idUsuario}
    res(confrmacionBD);
})


app.post('/api/media/likeImage', (req, res) => {
    let imageObject = req.body;
    // El imageObject traer�a el ID del usuario que public� la imagen, el ID de la imagen a la que se le dio like y
    // el ID del usuario que dio like a la imagen
    // imageObject: {idUserDaLike, idImagenRecibeLike}
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/media/highlightImage', (req, res) => {
    let imageObject = req.body;
    // El imageObject traer�a el ID de la imagen que quiere remarcar
    // Las im�genes que se remarcan son las que aparecen al inicio del perfil de la persona en la secci�n de im�genes
    // (sin necesidad de mover el slider)    
    // imageObject: {idImage}
    //post a la BD
    res(confrmacionBD);
})


//acciones para mensajer�a
app.post('/api/message/requestChat', (req, res) => {
    let messageObject = req.body;
    // El messageObject traer�a el ID del usuario que quiere iniciar el chat y el ID del usuario con quien quiere hablar
    // messageObject: {idUserSolicitaChat, idUserRecibeSolicitud}
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/message/acceptChatRequest', (req, res) => {
    let messageObject = req.body;
    // El messageObject traer�a el ID del usuario que acepta el chat request y el ID del usuario que envi� el chat request
    // messageObject: {idUserAceptaChat, idUserEnviaSolicitud}
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/message/viewMessages', (req, res) => {
    let messageObject = req.body;
    //esta funci�n es para cargar mensajes antiguos de una conversaci�n actual con otro perfil
    //se debe definir c�mo se van a hacer los chats para saber qu� par�metros tiene el messageObject
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/message/sendMessage', (req, res) => {
    let messageObject = req.body;
    // El messageObject traer�a el ID del usuario que env�a el mensaje y el ID del usuario que recibe el mensaje
    // messageObject: {idUserEnv�aMensaje, idUserRecibeMensaje}
    //post a la BD
    res(confrmacionBD);
})


app.post('/api/message/sendMedia', (req, res) => {
    let messageObject = req.body;
    // El messageObject traer�a el ID del usuario que env�a el mensaje, el ID del usuario que recibe el mensaje
    // y el ID del contenido multimedia que se va a enviar
    // messageObject: {idUserEnv�aMensaje, idUserRecibeMensaje, idMedia}
    //post a la BD
    res(confrmacionBD);
})


/* Para iniciar el express server */
app.listen(4000, () => console.log('Example app listening on port 4000!'))