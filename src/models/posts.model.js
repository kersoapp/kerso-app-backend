// posts-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const posts = new Schema({
    _userId: { type: Schema.Types.ObjectId, required: true },
    title: { type: String, default: ' ' },
    text: { type: String, required: true },
    videoUrl: { type: String, default: ' ' },
    imageUrl: { type: String, default: ' ' },
    likes: { type: Number, default: 0 },
    tags: [String],
    _usersRefs: [{ _uId: { type: Schema.Types.ObjectId }}]
  }, {
    timestamps: true
  });

  return mongooseClient.model('posts', posts);
};
