// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const users = new mongooseClient.Schema({
  
    email: {type: String, unique: true, required: true},
    password: { type: String, required: true},
    name: { type: String, default: ' '},
    displayName: { type: String, default: ' ' },
    photoUrl : { type: String, default: 'https://i.pinimg.com/originals/9f/00/af/9f00af501d2d324ac7f8ebb559fc25dd.png' },
    country : { type: String, default: ' ' },
    type : { type: String, required: true, enum: ['PLAYER','CLUB','SERVICES','UNIVERSITY_SERVICES','AGENT','TECHNICAL_STAFF','SPONSOR'] },
    position : { type: String, default: ' ' },
    year : { type: Number, default: 0 },
    urlCV : { type: String, default: ' ' },
    info : {
      age: { type: Number, default: 0 },
      height: { type: Number, default: 0 },
      weight: { type: Number, default: 0 },
      foot: { type: String, enum: ['R','L'], default: 'R' },
    },
    description: { type: String, default: ' ' },
    urlSocialNets: {
      facebook: { type: String, default: ' ' },
      twitter: { type: String, default: ' ' },
      instagram: { type: String, default: ' ' },
      linkedin: { type: String, default: ' ' },
      whatsapp: { type: String, default: ' ' },
    },
    followers: [{ type: Schema.Types.ObjectId }],
    following: [{ type: Schema.Types.ObjectId }]
  }, {
    timestamps: true
  });

  return mongooseClient.model('users', users);
};
