const url = require('url');
const MongoClient = require('mongodb').MongoClient;

const logger = require('./logger.js');

module.exports = function (app) {
  const config = app.get('mongodb');
  const dbName = url.parse(config).path.substring(1);
  const promise = MongoClient.connect(config).then(client => {
    // For mongodb <= 2.2
    if(client.collection) {
      return client;
    }
    
    return client.db(dbName);
  }).catch((error) => {
    logger.error('Error during MongoDB connection: ', app.get('mongodb'), error);
  });

  app.set('mongoClient', promise);
};
